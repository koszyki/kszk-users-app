"""
This module is responsible for the operation of 
token registration system. It is the public facing api.
"""
from kszk_users import ldap_api, token_database

class TokenServiceException(Exception):
    """Raised when an issue with the token service occurs."""

def add_candidate(name):
    """Creates a new candidate in database and returns its token."""
    if ldap_api.fetch_user(name):
        raise TokenServiceException("User already in LDAP: {}".format(name))
    if token_database.candidate_from_name(name):
        raise TokenServiceException(("Candidate already exists: "
                                    "{}".format(name)))
    return token_database.new_candidate(name)

def delete_candidate(name):
    """Removes a candidate with the given name."""
    try:
        token_database.delete_from_name(name)
    except ValueError:
        raise TokenServiceException(("Candidate does not exist: "
                                     "{}".format(name)))

def get_username_from_token(token):
    """Returns a candidate username from token, if exists."""
    candidate = token_database.candidate_from_token(token)
    if candidate:
        return candidate.name
    else:
        return None

def get_token_from_username(name):
    """Returns a candidate username from token, if exists."""
    candidate = token_database.candidate_from_name(name)
    if candidate:
        return candidate.token
    else:
        return None

def new_user(name, password, must_be_candidate=False):
    """Adds a user with the given password to LDAP and removes their 
    token.
    """
    try:
        token_database.delete_from_name(name)
    except ValueError:
        if must_be_candidate:
            raise TokenServiceException(("User {} is not a "
                                         "candidate.".format(name)))
    ldap_api.add_user(name, password)
