"""
This module contains methods used to manage passwords.
"""
from passlib.hash import ldap_sha512_crypt

def hash_(password):
    """Returns the hashed & salted password.

    PasswordHash interface:
        https://passlib.readthedocs.io/en/stable/lib/passlib.ifc.html#passwordhash-api
    SHA512 interface:
        https://passlib.readthedocs.io/en/stable/lib/passlib.hash.sha512_crypt.html
    LDAP wrapper:
        https://passlib.readthedocs.io/en/stable/lib/passlib.hash.ldap_crypt.html
    """
    return ldap_sha512_crypt.hash(password)

def validate(password, hash_):
    """Returns whether the given password corresponds to the hash.
    
    PasswordHash interface:
        https://passlib.readthedocs.io/en/stable/lib/passlib.ifc.html#passwordhash-api
    """
    return ldap_sha512_crypt.verify(password, hash_)
