"""
This module contains classes and functions related to 
accessing and modifying LDAP.
"""
import os
import logging
import io

import ldap
import ldap.modlist
import ldif

from .passwords import hash_

### Default variables and helper functions

class LDAPException(Exception):
    """Raised when something goes wrong with the request."""

class LDAPAuthException(LDAPException):
    """Raised when bind credentials are invalid."""

logger = logging.getLogger('kszk_users')
logger.setLevel(logging.WARNING)

DEFAULT_SERVER_ADDRESS = os.environ.get('LDAP_SERVER_ADDRESS', 'ldap://localhost')
if not DEFAULT_SERVER_ADDRESS.startswith('ldap://'):
    DEFAULT_SERVER_ADDRESS = 'ldap://' + DEFAULT_SERVER_ADDRESS
try:
    DEFAULT_SERVER_PORT = int(os.environ.get('LDAP_SERVER_PORT', 389))
except:
    logger.warning("Warning: LDAP_SERVER_PORT is not an integer. Assuming 389")
    DEFAULT_SERVER_PORT = 389
DEFAULT_ROOT_DN = os.environ.get('LDAP_ROOT_DN', 'dc=localhost')
DEFAULT_ADMIN_DN = os.environ.get('LDAP_ADMIN_DN', 'cn=admin,' +
                                  DEFAULT_ROOT_DN)
DEFAULT_ADMIN_PASSWORD = os.environ.get('LDAP_ADMIN_PASSWORD', 'pass')

def make_address(address=DEFAULT_SERVER_ADDRESS, port=DEFAULT_SERVER_PORT):
    if not address.startswith('ldap://'):
        address = 'ldap://' + address
    return "{}:{}".format(address, port)

def get_connection(address=DEFAULT_SERVER_ADDRESS, port=DEFAULT_SERVER_PORT,
                   bind_dn=DEFAULT_ADMIN_DN, bind_password=DEFAULT_ADMIN_PASSWORD):
    """Returns an authenticated LDAPObject."""
    connection = ldap.initialize(make_address(address, port))
    try:
        connection.bind_s(bind_dn, bind_password, ldap.AUTH_SIMPLE)
    except ldap.INVALID_CREDENTIALS:
        exc_string = "Invalid credentials for dn: {}".format(bind_dn)
        if bind_dn == DEFAULT_ADMIN_DN:
            exc_string += (". Did you set DEFAULT_ADMIN_DN and "
                           "DEFAULT_ADMIN_PASSWORD correctly?")
        raise LDAPAuthException(exc_string)
    except ldap.SERVER_DOWN:
        raise LDAPException("LDAP server is down.")
    return connection

def get_user_dn(uid, root_dn=DEFAULT_ROOT_DN):
    return "uid={},{}".format(uid, root_dn)

### LDAP functions

def add_user(uid, password, 
             root_dn=DEFAULT_ROOT_DN,
             address=DEFAULT_SERVER_ADDRESS, port=DEFAULT_SERVER_PORT,
             bind_dn=DEFAULT_ADMIN_DN, bind_password=DEFAULT_ADMIN_PASSWORD):
    """Adds a user with a given uid and password.
    
    Args:
        uid         user id
        password    password to be salted & hashed and stored
        root_dn     root dn
        address     address of the server
        port        port of the server
    """
    modlist = add_user_modlist(uid=uid, password=password, root_dn=root_dn)
    dn = get_user_dn(uid, root_dn=root_dn)
    connection = get_connection(address=address, port=port, 
                                bind_dn=bind_dn, bind_password=bind_password)
    try:
        connection.add_s(dn, modlist)
    except ldap.ALREADY_EXISTS:
        raise LDAPException("User already exists: {}".format(uid))

def add_user_modlist(uid, password, root_dn=DEFAULT_ROOT_DN,
                     hash_if_plaintext=True):
    """Returns an add modlist.

    Args:
        hash_if_plaintext   whether or not the password in the resulting
                            modlist should be hashed & salted if the given
                            password is plaintext
    """
    if not password.startswith('{CRYPT}'):
        if hash_if_plaintext:
            password = hash_(password)
        else:
            logger.warning("Warning: Password for {} is unencrypted".format(uid))
    entry = {
        'cn': [uid.encode('utf-8')],
        'sn': [uid.encode('utf-8')],
        'uid': [uid.encode('utf-8')],
        'objectClass': [b'inetorgperson'],
        'userPassword': [password.encode('utf-8')]
    }
    return ldap.modlist.addModlist(entry)

def fetch_user(uid, 
               root_dn=DEFAULT_ROOT_DN, 
               address=DEFAULT_SERVER_ADDRESS, port=DEFAULT_SERVER_PORT, 
               bind_dn=DEFAULT_ADMIN_DN, bind_password=DEFAULT_ADMIN_PASSWORD):
    """Returns a user's entry, if it exists. Otherwise, returns None."""
    connection = get_connection(address=address, port=port,
                                bind_dn=bind_dn, bind_password=bind_password)
    found = connection.search_s(root_dn, ldap.SCOPE_SUBTREE, '(uid={})'.format(uid))
    if len(found) == 1:
        return found[0][1]
    else:
        return None

def change_password(uid, new_password,
                    root_dn=DEFAULT_ROOT_DN, 
                    address=DEFAULT_SERVER_ADDRESS, port=DEFAULT_SERVER_PORT, 
                    bind_dn=DEFAULT_ADMIN_DN, bind_password=DEFAULT_ADMIN_PASSWORD,
                    hash_if_plaintext=True):
    """Changes a user's password."""
    if not new_password.startswith('{CRYPT}'):
        if hash_if_plaintext:
            new_password = hash_(new_password)
        else:
            logger.warning("Warning: Password for {} is unencrypted".format(uid))
    user_dn = get_user_dn(uid, root_dn=root_dn)
    connection = get_connection(address=address, port=port, 
                                bind_dn=bind_dn, bind_password=bind_password)
    modlist = [(ldap.MOD_REPLACE, 'userPassword', new_password.encode('utf-8'))]
    try:
        connection.modify_s(user_dn, modlist)
    except ldap.NO_SUCH_OBJECT:
        raise LDAPException("No such user: {}".format(uid))

def delete_user(uid, 
                root_dn=DEFAULT_ROOT_DN, 
                address=DEFAULT_SERVER_ADDRESS, port=DEFAULT_SERVER_PORT, 
                bind_dn=DEFAULT_ADMIN_DN, bind_password=DEFAULT_ADMIN_PASSWORD):
    """Deletes a user."""
    user_dn = get_user_dn(uid, root_dn=root_dn)
    entry = fetch_user(uid,
                       root_dn=root_dn,
                       address=address, port=port,
                       bind_dn=bind_dn, bind_password=bind_password)
    if entry:
        connection = get_connection(address=address, port=port,
                                    bind_dn=bind_dn, bind_password=bind_password)
        connection.delete_s(user_dn)
    else:
        raise LDAPException("No such user: {}".format(uid))

def authenticate_user(uid, password,
                      root_dn=DEFAULT_ROOT_DN, 
                      address=DEFAULT_SERVER_ADDRESS, port=DEFAULT_SERVER_PORT):
    """Returns whether the user's password is correct."""
    user_dn = get_user_dn(uid, root_dn=root_dn)
    try:
        get_connection(address=address, port=port,
                       bind_dn=user_dn, bind_password=password)
        return True
    except LDAPAuthException:
        return False

def list_users(root_dn=DEFAULT_ROOT_DN,
               address=DEFAULT_SERVER_ADDRESS, port=DEFAULT_SERVER_PORT, 
               bind_dn=DEFAULT_ADMIN_DN, bind_password=DEFAULT_ADMIN_PASSWORD):
    """Returns a list of usernames of all users."""
    connection = get_connection(address=address, port=port,
                                bind_dn=bind_dn, bind_password=bind_password)
    entry_list = connection.search_s(root_dn, ldap.SCOPE_SUBTREE, "(uid=*)")
    usernames = [entry[1]['uid'][0].decode('utf-8') for entry in entry_list]
    return usernames
