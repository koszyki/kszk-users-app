"""
This module is responsible for keeping track of registration tokens.
It should not be directly accessed from outside due to possible 
conflicts with LDAP.
"""
import os
import logging
import uuid

from sqlalchemy import create_engine as sqlalchemy_create_engine
from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session, relationship

logger = logging.getLogger('databases')
logger.setLevel(logging.WARNING)

DATABASE_ADDRESS = os.environ.get('POSTGRES_DATABASE_ADDRESS', 'localhost')
DATABASE_PORT = os.environ.get('POSTGRES_DATABASE_PORT', '5432')
DATABASE_NAME = os.environ.get('POSTGRES_DATABASE_NAME', 'kszk_users')
DATABASE_USER = os.environ.get('POSTGRES_DATABASE_USER', 'kszk_users')
DATABASE_PASSWORD = os.environ.get('POSTGRES_DATABASE_PASSWORD', 'kszk_users')

def create_engine(address=DATABASE_ADDRESS, port=DATABASE_PORT,
                  user=DATABASE_USER, password=DATABASE_PASSWORD,
                  name=DATABASE_NAME):
    """Returns an engine connected to a PostgreSQL database."""
    return sqlalchemy_create_engine('postgresql://{}:{}@{}:{}/{}'.format(user,
                                                                         password, 
                                                                         address,
                                                                         port,
                                                                         name))

def create_test_engine():
    """Returns an in-memory sqlite engine for testing."""
    return sqlalchemy_create_engine('sqlite:///:memory:')

def create_session(engine):
    logger.debug("Connecting to: {}".format(engine.url.database))
    Session = scoped_session(sessionmaker(bind=engine))
    return Session()

def create_default_session():
    return create_session(create_engine())

Base = declarative_base()

class Candidate(Base):
    __tablename__ = 'candidates'
    name = Column(String, primary_key=True)
    token = Column(String, unique=True, nullable=False)

def create_all_tables(engine):
    Base.metadata.create_all(engine)

def gen_token():
    """Returns a random token."""
    return str(uuid.uuid4())

def new_candidate(name, session=None):
    """Adds a new candidate to database and returns token."""
    if session is None:
        session = create_default_session()
    if candidate_from_name(name, session=session):
        raise ValueError("Candidate already in database: {}".format(name))
    token = gen_token()
    while candidate_from_token(token, session=session) is not None:
        token = gen_token()
    candidate = Candidate(name=name, token=token)
    session.add(candidate)
    session.commit()
    return token

def candidate_from_token(token, session=None):
    """Returns the Candidate with a given token, if exists. Otherwise, 
    returns None.
    """
    if session is None:
        session = create_default_session()
    return session.query(Candidate).filter_by(token=token).first()

def candidate_from_name(name, session=None):
    """Returns the Candidate with the given name, if exists. Otherwise, 
    returns None.
    """
    if session is None:
        session = create_default_session()
    return session.query(Candidate).filter_by(name=name).first()

def delete_from_token(token, session=None):
    """Delete a candidate with the given token."""
    if session is None:
        session = create_default_session()
    instance = session.query(Candidate).filter_by(token=token).first()
    if instance:
        session.delete(instance)
        session.commit()
    else:
        raise ValueError("Candidate with token does not exist: {}".format(token))

def delete_from_name(name, session=None):
    """Delete a candidate with the given name."""
    if session is None:
        session = create_default_session()
    instance = session.query(Candidate).filter_by(name=name).first()
    if instance:
        session.delete(instance)
        session.commit()
    else:
        raise ValueError("Candidate does not exist: {}".format(name))

def list_candidate_tuples(session=None):
    """Returns a list of (username, token) tuples of all candidates."""
    if session is None:
        session = create_default_session()
    instances = session.query(Candidate).all()
    return [(instance.name, instance.token) for instance in instances]
