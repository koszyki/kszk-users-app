FROM tiangolo/uwsgi-nginx-flask:python3.7

RUN apt-get update && apt-get install -y python-dev libssl-dev libldap2-dev libsasl2-dev

COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY ./app.py /app/main.py
COPY ./kszk_users /app/kszk_users
COPY ./templates /app/templates
COPY ./users-cli.py /usr/bin/users-cli.py
