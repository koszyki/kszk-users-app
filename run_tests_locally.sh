#!/usr/bin/bash
test_network=test-kszk_users-network
test_container_name_ldap=test_ldap
test_container_name_kszk_users=test_kszk_users

docker build . -t test_kszk_users

docker network create $test_network

docker run -d --rm --network $test_network \
    --env SLAPD_PASSWORD=test \
    --env SLAPD_DOMAIN=domain.test \
    --env SLAPD_ADDITIONAL_SCHEMAS=openldap \
    --hostname test_ldap --name $test_container_name_ldap dinkel/openldap 

# the entrypoint and command ensures it won't die instantly
docker run -d --rm --network $test_network \
    --env LDAP_SERVER_ADDRESS=test_ldap \
    --env LDAP_ADMIN_PASSWORD=test \
    --env LDAP_ROOT_DN=dc=domain,dc=test \
    --name $test_container_name_kszk_users \
    --entrypoint tail kszk_users -f /dev/null

docker cp tests test_kszk_users:/app

docker exec test_kszk_users pip3 install pytest pytest-cov
docker exec test_kszk_users python3 -m pytest --cov -vv tests

docker kill $test_container_name_ldap
docker kill $test_container_name_kszk_users
docker network rm $test_network
docker rmi test_kszk_users
