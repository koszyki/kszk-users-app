"""
These tests are meant to examine whether the LDAP api works.
"""
import random
import string

import pytest
from ldap import SERVER_DOWN as LDAPServerDownException
from ldap import INVALID_CREDENTIALS as LDAPInvalidCredentialsException

from kszk_users import ldap_api

SKIP_REASON = """Test server is down. There must be an OpenLDAP server running
on localhost with the admin dn being cn=admin,dc=localhost and
password being 'test'."""
try:
    ldap_api.get_connection(bind_password='test')
    SERVER_DOWN = False
except LDAPServerDownException:
    SERVER_DOWN = True
except LDAPInvalidCredentialsException:
    SERVER_DOWN = True

@pytest.fixture
def randomized_user():
    uid = ''.join(random.choices(string.ascii_lowercase, k=7))
    password = ''.join(random.choices(string.ascii_lowercase, k=7))
    return uid, password

def test_make_address():
    assert ldap_api.make_address(address='localhost', port=389) == 'ldap://localhost:389'
    assert ldap_api.make_address(address='ldap://localhost', port=389) == 'ldap://localhost:389'

def test_get_user_dn():
    assert ldap_api.get_user_dn('butch', root_dn='dc=localhost') == 'uid=butch,dc=localhost'

@pytest.mark.skipif(SERVER_DOWN, reason=SKIP_REASON)
def test_ldap_flow(randomized_user):
    """This test examines if all of the LDAP API functions
    are working provided that a test server is running on 
    localhost.
    """
    uid, passwd = randomized_user
    ldap_api.add_user(uid, passwd, bind_password='test')
    hopefully_a_user = ldap_api.fetch_user(uid,
                                         bind_password='test')
    assert ldap_api.authenticate_user(uid, passwd)
    assert hopefully_a_user is not None
    old_password_hash = hopefully_a_user['userPassword'][0]
    ldap_api.change_password(uid, 'sneaky', bind_password='test')
    duck = ldap_api.fetch_user(uid, bind_password='test')
    new_password_hash = duck['userPassword'][0]
    assert old_password_hash != new_password_hash
    assert ldap_api.authenticate_user(uid, 'sneaky')
    assert new_password_hash.startswith(b'{CRYPT}')
    assert uid in ldap_api.list_users(bind_password='test')
    ldap_api.delete_user(uid, bind_password='test')
    assert ldap_api.fetch_user(uid, bind_password='test') is None

@pytest.mark.skipif(SERVER_DOWN, reason=SKIP_REASON)
def test_user_errors():
    """This tests if the api handles errors correctly."""
    assert not ldap_api.authenticate_user('i_dont_know', 'something')
    try: # just make sure to remove this user
        ldap_api.delete_user('somebody', bind_password='test')
    except:
        pass
    assert ldap_api.fetch_user('somebody', bind_password='test') is None
    try:
        ldap_api.change_password('somebody', 'a_password', bind_password='test')
        assert False
    except ldap_api.LDAPException:
        pass
    try:
        ldap_api.delete_user('somebody', bind_password='test')
        assert False
    except ldap_api.LDAPException:
        pass
    ldap_api.add_user('somebody', 'password', bind_password='test')
    try:
        ldap_api.add_user('somebody', 'password', bind_password='test')
        assert False
    except ldap_api.LDAPException:
        pass
    try:
        ldap_api.add_user('somebody', 'password', bind_password='testtteroni')
        assert False
    except ldap_api.LDAPAuthException:
        pass
    ldap_api.delete_user('somebody', bind_password='test')
