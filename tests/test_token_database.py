"""
These tests are meant to examine whether the token database works.
"""
import random
import string

import pytest

from kszk_users import token_database

def random_string(k=6):
    return ''.join(random.choices(string.ascii_lowercase, k=k))

@pytest.fixture
def clean_session():
    engine = token_database.create_test_engine()
    token_database.create_all_tables(engine)
    return token_database.create_session(engine)

def test_database_workflow(clean_session):
    token_database.new_candidate('mr', session=clean_session)
    candidate = token_database.candidate_from_name('mr', session=clean_session)
    assert candidate is not None
    assert candidate.name == 'mr'
    assert (token_database.candidate_from_token(
        candidate.token, session=clean_session).name == candidate.name)
    try:
        token_database.new_candidate('mr', session=clean_session)
        assert False
    except ValueError:
        pass
    token_database.delete_from_name('mr', session=clean_session)
    assert token_database.candidate_from_name('mr', session=clean_session) is None
    try:
        token_database.delete_from_name('mr', session=clean_session)
        token_database.delete_from_token('mr', session=clean_session)
        assert False
    except ValueError:
        pass
    strings = [random_string() for i in range(5)]
    for str_ in strings:
        token_database.new_candidate(str_, session=clean_session)
    tuples_in_database = token_database.list_candidate_tuples(session=clean_session)
    assert all(str_ in list(zip(*tuples_in_database))[0] for str_ in strings)
