"""
These tests are meant to examine whether password hashing works.
"""
import pytest

from kszk_users import passwords

@pytest.fixture
def sample_password():
    return 'oman'

@pytest.fixture
def sample_hash():
    return "{CRYPT}$6$rounds=656000$Wcf5Oh.clk.z8SMS$VO/ZOiUhuI8uM8dzNb/p/n6Bz1Namt.ahPYit7pQ7IYRbMKXOJoFCtR1poJTqPMA3J7TnYD7PvmSOR3EpUXSd0"

def test_validation(sample_password, sample_hash):
    assert passwords.validate(sample_password, sample_hash)

def test_hash(sample_password):
    hash_ = passwords.hash_(sample_password)
    assert hash_.lower().startswith('{crypt}')
    assert hash_ != sample_password
    assert passwords.validate(sample_password, hash_)
