"""
This is a basic flask app that allows for simple one time 
registering & password changes.
"""
from flask import Flask, request

from kszk_users import service, token_database

app = Flask(__name__)

def parse_template(path, **kwargs):
    with open(path) as file_:
        str_ = file_.read()    
        for from_, to in kwargs.items():
            str_ = str_.replace("{%"+from_+"%}", to)
        return str_

@app.route('/', methods=['GET', 'POST'])
def welcome():
    if request.method == 'GET':
        token = request.args.get('token')
        username = service.get_username_from_token(token)
        if token:
            if not username:
                return parse_template('templates/wrong_token.html', token=token)
            elif username:
                return parse_template('templates/register.html',
                                      username=username, token=token)
        else:
            return parse_template('templates/blank.html')
    if request.method == 'POST':
        token = request.form.get('token')
        password = request.form.get('password')
        if (not token) or (not password):
            return parse_template('templates/error.html')
        username = service.get_username_from_token(token)
        if not username:
            return parse_template('templates/wrong_token.html', token=token)
        service.new_user(username, password, must_be_candidate=True)
        return parse_template('templates/complete.html', username=username)

engine = token_database.create_engine()
token_database.create_all_tables(engine)
