#!/usr/bin/env python3
"""
users-cli.py
============
Script to perform user account management in the kszk user flow.

It makes use of these environment variables as implicit arguments [default values]:
LDAP_SERVER_ADDRESS         [localhost]
LDAP_SERVER_PORT            [389]
LDAP_ROOT_DN                [dc=localhost]
LDAP_ADMIN_DN               [cn=admin,dc=localhost]
LDAP_ADMIN_PASSWORD         [test]
POSTGRS_DATABASE_ADDRESS    [localhost]
POSTGRS_DATABASE_PORT       [5432]
POSTGRS_DATABASE_NAME       [kszk_users]
POSTGRS_DATABASE_USER       [kszk_users]
POSTGRS_DATABASE_PASSWORD   [kszk_users]
URL_ROOT                    [http://localhost:5000/]

Usage:
    users-cli.py (add_user | delete_user | change_password | authenticate_user) <username> [<password>]
    users-cli.py (add_candidate | delete_candidate) <username>
    users-cli.py list (users | candidates)
"""
import os
from getpass import getpass
import warnings

from docopt import docopt

from kszk_users import ldap_api
from kszk_users import service, token_database

warnings.filterwarnings("ignore", category=UserWarning, module='psycopg2')

opts = docopt(__doc__, help=True, version=("kszk.eu LDAP user management script "
                                           "v0.1.0"))

URL_ROOT = os.environ.get('URL_ROOT', 'http://localhost:5000/')
def token_url(token):
    return URL_ROOT + "?token={}".format(token)

if ((opts['add_user'] or opts['change_password'] or opts['authenticate_user'])
        and not opts['<password>']):
    possible_password = getpass("Please input password for the user: ")
    if getpass("Repeat password: ") == possible_password:
        opts['<password>'] = possible_password
    else:
        print("Passwords do not match")
        exit(1)

try:
    if opts['add_user']:
        service.new_user(opts['<username>'], opts['<password>'])
    if opts['delete_user']:
        ldap_api.delete_user(opts['<username>'])
    if opts['change_password']:
        ldap_api.change_password(opts['<username>'], opts['<password>'])
    if opts['authenticate_user']:
        is_authenticated = ldap_api.authenticate_user(opts['<username>'],
                                                      opts['<password>'])
        if is_authenticated:
            print("Credentials are valid!")
        else:
            print("Credentials are invalid.")
    if opts['add_candidate']:
        token = service.get_token_from_username(opts['<username>'])
        if token is not None:
            print("Candidate already exists. \nToken: {}".format(token))
        else:
            token = service.add_candidate(opts['<username>'])
            print("Candidate {} created. \nURL: {}".format(
                opts['<username>'], token_url(token)))
    if opts['delete_candidate']:
        service.delete_candidate(opts['<username>'])
    if opts['list']:
        if opts['users']:
            names = ldap_api.list_users()
            print("Users:")
            for name in names:
                print("  {:<10}".format(name))
        if opts['candidates']:
            candidate_tuples = token_database.list_candidate_tuples()
            print("Candidates:")
            for name, token in candidate_tuples:
                print("  {:<10}\t{}".format(name, token_url(token)))
except (ldap_api.LDAPException, service.TokenServiceException) as exc:
    print("Error:", exc)
